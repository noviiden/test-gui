package gui.dto;

/**
 * Created by Nikita Navalikhin on 27/07/16.
 */
public class ContentApi {
    private String id;
    private String publishedAt;
    private String title;
    private JournalContent journalContent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public JournalContent getJournalContent() {
        return journalContent;
    }

    public void setJournalContent(JournalContent journalContent) {
        this.journalContent = journalContent;
    }
}
