package gui.dto;

/**
 * Created by Nikita Navalikhin on 27/07/16.
 */
public class JournalTo {
    private String id;
    private String publishedAt;
    private String title;
    private String name;
    private String path;


    public JournalTo(String id, String publishedAt, String title, String name, String path) {
        this.id = id;
        this.publishedAt = publishedAt;
        this.title = title;
        this.name = name;
        this.path = path;
    }

    public String getByIndex(int index){
        String val = null;

        if(index == 0){
            val = id;
        }
        if(index == 1){
            val = publishedAt;
        }
        if(index == 2){
            val = title;
        }
        if(index == 3){
            val = name;
        }
        if(index == 4){
            val = path;
        }

        return val;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
