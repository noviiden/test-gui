package gui.dto;

/**
 * Created by Nikita Navalikhin on 27/07/16.
 */
public class JournalContent {
    private String name;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
