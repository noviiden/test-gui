package gui.dto;

import java.util.List;

/**
 * Created by Nikita Navalikhin on 27/07/16.
 */
public class Atricles {
    private List<ContentApi> content;

    public List<ContentApi> getContent() {
        return content;
    }

    public void setContent(List<ContentApi> content) {
        this.content = content;
    }
}
