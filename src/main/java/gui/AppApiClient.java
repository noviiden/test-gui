package gui;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import gui.dto.Atricles;
import gui.dto.ContentApi;
import gui.dto.JournalTo;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Collection;

public class AppApiClient {

    private static final String[] schema = {"id", "publishedAt", "title", "name", "path"};
    private static final String[] columnNames = {"id", "publishedAt", "title", "name", "path"};
    private static final Class<?>[] classes = new Class<?>[columnNames.length];
    static {
        for (int i=0; i<columnNames.length; i++) {
            classes[i] = String.class;
        }
    }

    final private AppView view;
    final private AppTable table;

    OkHttpClient client = new OkHttpClient();

    public AppApiClient(String apiHost, AppView _view) {
        this.view = _view;
        this.table = new AppTable(schema, columnNames, classes);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                view.setModel(table);
            }
        });

        loading(apiHost);
    }

    private void loading(final String apiHost){
        view.changeStatus("loading...");
        new Thread(){
            @Override
            public void run() {
                Request request = new Request.Builder().url(apiHost + "/journal-article?userId=1").build();
                try {
                    Response response = client.newCall(request).execute();
                    String json = response.body().string();
                    view.changeStatus("loading ok");

                    //***ДЛЯ ТЕСТА МОКАЕМ РЕЗУЛАЬТАТ

                    json = "{\n" +
                            "  \"content\": [\n" +
                            "    {\n" +
                            "      \"id\": 0,\n" +
                            "      \"journalContent\": {\n" +
                            "        \"mediaType\": \"string\",\n" +
                            "        \"name\": \"string\",\n" +
                            "        \"path\": \"string\",\n" +
                            "        \"size\": 0\n" +
                            "      },\n" +
                            "      \"publishedAt\": \"string\",\n" +
                            "      \"title\": \"string\"\n" +
                            "    }\n" +
                            "  ],\n" +
                            "  \"first\": true,\n" +
                            "  \"last\": true,\n" +
                            "  \"number\": 0,\n" +
                            "  \"numberOfElements\": 0,\n" +
                            "  \"size\": 0,\n" +
                            "  \"sort\": {},\n" +
                            "  \"totalElements\": 0,\n" +
                            "  \"totalPages\": 0\n" +
                            "}";

                    Gson gson = new Gson();

                    Atricles result = gson.fromJson(json, Atricles.class);

                    Collection<JournalTo> data = Collections2.transform(result.getContent(), new Function<ContentApi, JournalTo>() {
                        @Override
                        public JournalTo apply(ContentApi contentApi) {
                            return new JournalTo(contentApi.getId(), contentApi.getPublishedAt(), contentApi.getTitle(), contentApi.getJournalContent().getName(), contentApi.getJournalContent().getPath());
                        }
                    });

                    table.setValueAt(Lists.newArrayList(data));

                } catch (IOException e) {
                    e.printStackTrace();
                    view.changeStatus("error with response");
                }
            }
        }.start();
    }


}
