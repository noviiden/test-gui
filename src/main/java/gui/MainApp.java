package gui;

import javax.swing.SwingUtilities;

public class MainApp {

    private static final long serialVersionUID = 1L;

    public static void main(final String[] args) {
        //args[0] should contain the address of the server (e.g.: http://localhost:8080 )

        final String apiHost = args.length > 0 ? args[0] : "http://localhost:8080";

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                AppView view = new AppView();
                view.initialize();
                new AppApiClient(apiHost, view);
            }
        });
    }

    
} 
