
package gui;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

public class AppView extends JFrame {

    private static final long serialVersionUID = 1L;
    
    private static final String TITLE = "AppView :: Testing";
    private static final ImageIcon LOGO = new ImageIcon(AppView.class.getClassLoader().getResource("images/logo.png"));
    
    private JTable table;
    private JLabel statusLabel;

    private boolean initialized = false;

    //private static ShutDownThread shutDownThread;

    public AppView() {
        super();
    }

    public synchronized void initialize() {
        if (this.initialized) {
            return;
        }
        this.initialized = true;
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //prepares the data-JTable and the status-JLabel then initiate the JContentPane
        
        this.table = new JTable();
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        //we use our custom renderer to paint the cells
        table.setDefaultRenderer(String.class, new HighlightTableCellRenderer());
        
        this.statusLabel = new JLabel("DISCONNECTED");
        this.statusLabel.setToolTipText("DISCONNECTED");

        this.setSize(850, 625);
        this.setContentPane(createJContentPane(this.statusLabel,this.table));
        this.setTitle(TITLE);
    
        this.setVisible(true);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                int row = table.getSelectedRow();
                int col = table.getSelectedColumn();

                super.mouseClicked(e);
            }
        });
    }
    
    private static JPanel createJContentPane(JLabel statusLabel, JTable table) {
        //this is the main JPanel
        JPanel jContentPane = new JPanel();
        jContentPane.setBackground(Color.white);
        jContentPane.setLayout(new BoxLayout(jContentPane,BoxLayout.Y_AXIS));
        
        //we create a JPanel containing the status indicator, our logo and the demo name
        //we'll use this JPanel as first element on the main JPanel
        JPanel firstLine = new JPanel();
        firstLine.setBackground(Color.white);
        firstLine.setLayout(new BoxLayout(firstLine,BoxLayout.X_AXIS));
        
        firstLine.add(statusLabel);
        
        JLabel label = new JLabel(TITLE,LOGO,JLabel.LEFT);
        firstLine.add(label);
        
        firstLine.add(Box.createHorizontalGlue());
   
        //then we add the previously created first-line-JPanel and the JTable to
        //the main JPanel
        jContentPane.add(firstLine);
        //(we wrap the JTable in a JSCrollPane so that we have scrollbars)
        JScrollPane scrollPane = new JScrollPane(table);
        jContentPane.add(scrollPane);
                
        return jContentPane;
    }
    
    public synchronized void setModel(AppTable model) {
        //we pass the model to the JTable
        this.table.setModel(model);
        //we set the first column to be larger (it contains the stock_name)
        TableColumn col = table.getColumnModel().getColumn(0); 
        col.setPreferredWidth(250); 
        
        //we add a sorter to the JTable (and we listen for sort changes, see below)
        TableRowSorter<AppTable> trs = new TableRowSorter<AppTable>(model);
        trs.addRowSorterListener(new SortListener());
        
        table.setRowSorter(trs);
    }
    
    public synchronized void enableDynamicSort(boolean enabled) {
        //enable or disable the dynamic sort. When dynamic sort is ON we always need that the model, each times it
        //receives an update, asks the view for a complete refresh, so to enable the SortsOnUpdates we have also to 
        //enable the complete refresh on each update
        ((TableRowSorter<?>) table.getRowSorter()).setSortsOnUpdates(enabled);
    }
    
    public synchronized void changeStatus(String status) {
        //changes connection status icon and tooltip text
        statusLabel.setToolTipText(status);
        statusLabel.setText(status);
    }
    
    public class SortListener implements RowSorterListener {
        @Override
        public synchronized void sorterChanged(RowSorterEvent e) {
            //the sort listener waits for changes on the sorted columns
            //if the first sort key is the stock_name or the open_price column
            //(that are fields that never change) it disable the dynamic sort
            if (e.getType().equals(RowSorterEvent.Type.SORT_ORDER_CHANGED)) {
                List<RowSorter.SortKey> keys = e.getSource().getSortKeys();
                if(!keys.isEmpty()) {
                    int y = keys.get(0).getColumn();
                    if (y != 0 && y != 11) {
                        enableDynamicSort(true);
                    } else {
                        enableDynamicSort(false);
                    }
                }
            } 
        }
    }
        
    public class HighlightTableCellRenderer extends DefaultTableCellRenderer {

        private static final long serialVersionUID = 7837295534229006872L;
        
        private final Color oddRow = new Color(238,238,238);
        private final Color evenRow = Color.white;
        
        private final Color oddHot = Color.yellow;
        private final Color evenHot = new Color(255,255,100);
        
        //note that column and rows received by this method are view-related, so we'll use the convertColumnIndexToModel
        //to convert them when wee nedd'em to be model-related
        @Override
        public synchronized Component getTableCellRendererComponent (JTable table, Object value, boolean selected, boolean focused, int row, int column) {
            //all the columns but the stock_name one have to be aligned on the right
            if(table.convertColumnIndexToModel(column) != 0) {
                this.setHorizontalAlignment(RIGHT);
            } else {
                this.setHorizontalAlignment(LEFT);
            }
            //we check on the UpdateString if the value is hot or cold and we set the background color
            //accordingly; in case the field is null or is not an UpdatString we use the cold colors
            if (value != null) {
                if ((row % 2) == 0) {
                    this.setBackground(evenHot);
                } else {
                    this.setBackground(oddHot);
                }
                
            } else {
                if ((row % 2) == 0) {
                    this.setBackground(evenRow);
                } else {
                    this.setBackground(oddRow);
                } 
            }

            if(column == 0){
                JButton link = new JButton(value.toString());
                link.setMaximumSize(new Dimension(10,10));
                link.setActionCommand("1");
                return link;
            }else{
                return super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            }
        }
        
    }


}  
