
package gui;


import gui.dto.JournalTo;

import javax.swing.table.AbstractTableModel;
import java.awt.event.MouseAdapter;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class AppTable extends AbstractTableModel {

    private static final long serialVersionUID = -7846472930012885468L;

    private String[] schema;
    private String[] columnNames;
    private Class<?>[] classes;

    //the actual model
    private List<JournalTo> data;

    public AppTable(String[] schema, String columnNames[], Class<?>[] classes) {
        this.schema = schema;
        this.columnNames = columnNames;
        this.classes = classes;

        if (columnNames.length != schema.length || classes.length != columnNames.length) {
            throw new InvalidParameterException("schema columNames and classes must be of the same length");
        }
        this.data = new ArrayList<>();
    }

    public void setValueAt(List<JournalTo> data) {
        this.data = data;
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int rowIndex, int columIndex) {
        return this.data.get(rowIndex).getByIndex(columIndex);
    }

    @Override
    public int getColumnCount() {
        return this.schema.length;
    }

    @Override
    public String getColumnName(int columIndex) {
        return this.columnNames[columIndex];
    }

    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public Class<?> getColumnClass(int columIndex) {
        return this.classes[columIndex];
    }


}
